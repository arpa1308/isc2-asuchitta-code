﻿using System;
using System.Linq;

namespace PubSubPattern
{
    /// <summary>
    /// A simple Pub/Sub pattern implementation.
    /// </summary>
    public sealed class PubSubService
    {
        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        static PubSubService()
        {
        }

        private PubSubService()
        {
        }

        /// <summary>
        /// A singleton for service interaction.
        /// </summary>
        public static PubSubService Instance { get; } = new PubSubService();

        /// <summary>
        /// Subscribes a given IHandleMessage implementation to the channels it returns.
        /// </summary>
        /// <param name="implementation">An instance of IHandleMessage.</param>
        /// <exception cref="ArgumentNullException">Throws ArgumentNullException if implementation is null</exception>
        public void Subscribe(IHandleMessage implementation)
        {
            if (implementation == null)
            {
                throw new ArgumentNullException("implementation is null.");
            }

            if (!ProcessedLeadsStorage.SubscribedChannel.Any() || !ProcessedLeadsStorage.SubscribedChannel.Contains(implementation.Subscriber))
            {
                ProcessedLeadsStorage.SubscribedChannel.Add(implementation.Subscriber);
            }
        }

        /// <summary>
        /// Unsubscribes a given IHandleMessage implementation to the channels it returns.
        /// </summary>
        /// <param name="implementation">An instance of IHandleMessage.</param>
        /// <exception cref="ArgumentNullException">Throws ArgumentNullException if implementation is null</exception>
        public void Unsubscribe(IHandleMessage implementation)
        {
            if (implementation == null)
            {
                throw new ArgumentNullException("implementation is null.");
            }
            if (ProcessedLeadsStorage.SubscribedChannel.Any() && ProcessedLeadsStorage.SubscribedChannel.Contains(implementation.Subscriber))
            {
                ProcessedLeadsStorage.SubscribedChannel.Remove(implementation.Subscriber);
            }

        }

        /// <summary>
        /// Publishes a message to a given channel containing the specified data.
        /// </summary>
        /// <param name="channel">The channel to emit a message on.</param>
        /// <param name="data">The data to emit.</param>
        /// <exception cref="ArgumentNullException">Throws ArgumentNullException if channel is null.</exception>
        /// <exception cref="ArgumentNullException">Throws ArgumentNullException if data is null.</exception>
        public void Publish(string channel, object data)
        {
            if (channel == null)
            {
                throw new ArgumentNullException("channel is null.");
            }
            if (data == null)
            {
                throw new ArgumentNullException("data is null.");
            }

            if (!channel.Equals(IncomingLeadHandler.INCOMING_LEAD_CHANNEL)) return;
            foreach (var c in ProcessedLeadsStorage.SubscribedChannel)
            {
                var lead = data as Lead;
                if (string.IsNullOrEmpty(lead.FirstName))
                {
                    throw new ArgumentException("First Name empty.");
                }
                ProcessedLeadsStorage.ProcessedLeads.Add(lead);
            }
        }
    }
}
