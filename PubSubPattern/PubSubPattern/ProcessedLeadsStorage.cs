﻿using System;
using System.Collections.Generic;

namespace PubSubPattern
{
    public static class ProcessedLeadsStorage
    {
        static ProcessedLeadsStorage()
        {
            if (ProcessedLeads == null)
            {
                ProcessedLeads = new List<Lead>();
            }
            if (SubscribedChannel == null)
            {
                SubscribedChannel = new List<string>();
            }

        }

        public static List<Lead> ProcessedLeads { get; set; }

        public static List<string> SubscribedChannel { get; set; }
    }
}
